export interface ErrorPageDetails {
    errorCode?:Number | string,
    image?:string | undefined,
    content?:string

}