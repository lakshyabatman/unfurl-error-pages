import {ErrorPageDetails} from './data.model'

export const HttpErrors: Array<ErrorPageDetails> = [
    {
        errorCode:404,
        image:'hiking.png',
        content:'Oops! There’s been some mistake. The page you were looking for was not found.'
    },
    {
        errorCode:401,
        image:'authentication.png',
        content:'Oops! There’s been some mistake. The page you were looking for was not found.'
    },
    {
        errorCode:500,
        image:'data_process.png',
        content:'Oh darn it! We’ve experienced an internal server error. Please try again or report this error.'
    },
    {
        errorCode:403,
        image:'iphone.png',
        content:'You’re forbidden from accessing this! Please report if there’s been an error or contact the administrator. '
    }
    
]

export const GenericErrors: Array<ErrorPageDetails> =[
    {
        errorCode:'Oh no!',
        image:'problem_solving.png',
        content:'We’ve encountered some error. You can report this error or head back to home. Be assured, we’re working on minimizing such errors!'
    },
    {
        errorCode:'Oops',
        image:'cleaning_pc.png',
        content:'You’ve caught us doing some maintenance work. Please be patient. We’ll be back up very soon! '
    },
]